#!/bin/sh

DIST=stretch

commands="bsdtar apt-move apt-ftparchive xorriso"
isofile="$1"
output=custom-${isofile#*-}

clean_up() {
  [ -e staging/cd ] && chmod -R +w staging/cd
  rm -rf staging
}

trap 'clean_up' EXIT
trap 'clean_up; exit 1' INT TERM

if [ -z "$isofile" ]; then
  echo "usage: $0 iso" >&2
  exit 1
fi

for cmd in $commands; do
  found=true
  if ! which $cmd >/dev/null 2>&1; then
    found=false
    echo "$0: $cmd: command not found" >&2
  fi
done
if ! $found; then
  exit 1
fi

[ -e staging/cd ] && chmod -R +w staging/cd
rm -rf staging
mkdir -p staging

sed \
  -e '/^DIST=/s,\(=\).*,\1'"$DIST"',' \
  -e '/^COPYONLY=/s,\(=\).*,\1yes,' \
  -e '/^LOCALDIR=/s,\(=\).*,\1'`pwd`'/staging/localdir,' \
  -e '/^FILECACHE=/s,\(=\).*,\1'`pwd`'/cache,' \
  conf/apt-move.conf.in >staging/apt-move.conf

sed \
  -e '/@DIST@/s/@DIST@/'"$DIST"'/g' \
  conf/config-deb.in >staging/config-deb

mkdir -p staging/localdir
apt-move -c staging/apt-move.conf get
apt-move -c staging/apt-move.conf move
apt-move -c staging/apt-move.conf packages

mkdir -p staging/cd
bsdtar -C staging/cd -xf "$isofile"
chmod -R +w staging/cd

cp -pr staging/localdir/pool/main/* staging/cd/pool/main/
apt-ftparchive generate staging/config-deb

sed -i '/MD5Sum:/,$d' staging/cd/dists/$DIST/Release
apt-ftparchive release staging/cd/dists/$DIST >>staging/cd/dists/$DIST/Release
cd staging/cd
  find -L . ! -name "md5sum.txt" ! -path "./isolinux/*" -type f -exec md5sum {} + >md5sum.txt
cd - >/dev/null

chmod -R -w staging/cd
dd if=$isofile of=staging/isohdpfx.bin bs=1 count=432
xorriso -as mkisofs -o $output \
  -isohybrid-mbr staging/isohdpfx.bin \
  -c isolinux/boot.cat -b isolinux/isolinux.bin \
  -no-emul-boot -boot-load-size 4 -boot-info-table \
  ./staging/cd

md5sum $output >$output.md5
