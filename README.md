## Description

A script to build Debian ISO with additional packages.

## Instructions

Place all the `*.deb` files you wish to be added to the ISO into `cache` directory.

Run `create_cd.sh debian-x.x.x-amd64-netinst.iso`.

## Dependencies

* libarchive-tools (provides `bsdtar`)
* apt-move
* apt-utils (provides `apt-ftparchive`)
* xorriso
